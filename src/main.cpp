/** SWA - Smith-Waterman Alignment
 *
 * @author The BioloGeeks Team
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

#include "../include/fasta.hpp"
#include "../include/align.hpp"


int main(int argc, char **argv) {
    if (argc <= 1) {
        std::cerr << "Usage:" << argv[0] << " <fasta-file>" << std::endl;
        return EXIT_FAILURE;
    }
    std::string filename(argv[1]);
    std::ifstream input(filename);
    if (!input.good()) {
        std::cerr << "Error opening '" << filename << "'" << std::endl; 
        return EXIT_FAILURE;
    }
    std::vector<Sequence> sequences = fasta::read_file(input);
    for (auto sequence: sequences) {
        CLUTCHLOG(info, ">" << sequence.name);
        CLUTCHLOG(info, sequence.seq);
    }
    std::string seq1 = sequences.at(0).seq;
    std::string seq2 = sequences.at(1).seq;
    std::vector scoring_matrix = align::scoring_matrix(seq1, seq2);
    CLUTCHLOG(info, "scoring matrix size:" << scoring_matrix.size());
    print_matrix(scoring_matrix, seq1.length()+1, seq2.length()+1);
    std::vector aligned_strings = align::alignment_traceback(seq1, seq2, scoring_matrix);
    std::cout << aligned_strings.at(0) << std::endl;
    std::cout << aligned_strings.at(1) << std::endl;
    return EXIT_SUCCESS;
}