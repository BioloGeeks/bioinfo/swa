/** Smith-Waterman local sequence alignment
 * @see https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm
 */
#pragma once

#include <algorithm>
#include <exception>
#include <initializer_list>
#include <vector>
#include <stdexcept>

#include "fasta.hpp"
#include "../vendor/clutchlog.h"

unsigned int argmax(std::vector<float> vector) {
    return std::distance(vector.begin(), std::max_element(vector.begin(), vector.end()));
}

void print_matrix(std::vector<float> &matrix, unsigned int nrow, unsigned int ncol) {
    for (unsigned int i=0; i < nrow; i++) {
        for (unsigned int j=0; j < ncol; j++) {
            std::cout << matrix[i*ncol+j] << " ";
        }
        std::cout << std::endl;
    }
}

namespace align {
    const float GAP_OPENING_PENALTY = 10;
    const float GAP_EXTENSION_PENALTY = 0.5;
    const float MISMATCH_PENALTY = -1;
    const float MATCH_PENALTY = 1; 

    float match_score(char nucleotide_a, char nucleotide_b) {
        if (nucleotide_a == nucleotide_b) {
            return MATCH_PENALTY; 
        } else {
            return MISMATCH_PENALTY;
        }
    }


    unsigned int gap_score(unsigned int gap_length) {
        return GAP_OPENING_PENALTY + gap_length * GAP_EXTENSION_PENALTY;
    }


    std::vector<float> scoring_matrix(std::string seq1, std::string seq2) {
        unsigned int m = seq1.length()+1;
        unsigned int n = seq2.length()+1;
        std::vector<float> matrix(n*m);
        for (unsigned int i = 1; i < n; i++) {
            char nucleotide_seq1 = seq1.at(i-1);
            for (unsigned int j = 1; j < m; j++) {
                char nucleotide_seq2 = seq2.at(j-1);
                float substitution_score = matrix[(i-1)*m+(j-1)] + match_score(nucleotide_seq1, nucleotide_seq2);
                float gap_seq1 = matrix[(i-1)*m+j] + GAP_EXTENSION_PENALTY;
                float gap_seq2 = matrix[(i)*m+(j-1)] + GAP_EXTENSION_PENALTY;
                std::vector<float> score_candidates = {0, substitution_score, gap_seq1, gap_seq2};
                float score = *std::max_element(score_candidates.begin(), score_candidates.end());
                matrix[i*m+j] = score;
            }
        }
        return matrix;
    }


    std::vector<std::string> alignment_traceback(std::string seq1, std::string seq2, std::vector<float> score_matrix) {
        unsigned int max_position = argmax(score_matrix);
        unsigned int m = seq1.length() + 1;
        unsigned int n = seq2.length() + 1;
        unsigned int i = max_position / m;
        unsigned int j = max_position % m;
        CLUTCHLOG(info, "i:" << i << ", j: " << j);
        std::string aligned_seq1 = "";
        std::string aligned_seq2 = "";
        float score = score_matrix[i*n+j];
        if (score_matrix.size() < m*n) {
            throw std::invalid_argument("Error: scoring matrix dimension is not consistent with sequence lengths ");
        }
        while (score != 0 && i > 0 && j > 0) {
            CLUTCHLOG(info, "score:" << score);
            float substitution_score = score_matrix[(i-1)*m+(i-1)] + match_score(seq1.at(i-1), seq2.at(j-1));
            float gap_seq1 = score_matrix[i*m+(j-1)] + GAP_EXTENSION_PENALTY;
            float gap_seq2 = score_matrix[(i-1)*m+j] + GAP_EXTENSION_PENALTY;
            if (score == substitution_score) {
                aligned_seq1 = seq1.at(i-1) + aligned_seq1;
                aligned_seq2 = seq2.at(j-1) + aligned_seq2;
                i--;
                j--;
            } else if (score == gap_seq1) {
                aligned_seq1 = '-' + aligned_seq1;
                aligned_seq2 = seq2.at(j-1) + aligned_seq2;
                j--;
            } else if (score == gap_seq2) {
                aligned_seq1 = seq1.at(i-1) + aligned_seq1;
                aligned_seq2 = '-' + aligned_seq2;
                i--;
            } else {
                throw std::invalid_argument("Error: the Smith-Waterman traceback encountered a strange jump between two adjacent scores");
            }
            score = score_matrix[i*m+j];
        }
        return {aligned_seq1, aligned_seq2};
    }
}