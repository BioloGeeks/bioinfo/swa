/** Simple FASTA file format reader
 * 
 * @author The BioloGeeks Team
 * @date 2023-09-30
 */
#pragma once

#include <iostream>
#include <fstream>
#include <vector>

class Sequence {
public: 
    Sequence(std::string name, std::string seq) {
        this->name = name;
        this->seq = seq;
    }

    std::string name, seq;
};

namespace fasta {

    /**
     * @see https://www.rosettacode.org/wiki/FASTA_format?section=10#C++
     * 
     */
    std::vector<Sequence> read_file(std::ifstream &input) {
        std::string line, name, content;
        std::vector<Sequence> sequences;
        // Read fasta file lines and append each FASTA records
        // to the sequences vector.
        while (input.good()) {
            std::getline(input, line);
            if (line[0] == '>') {
                if (!name.empty()) {
                    Sequence sequence(name, content);
                    sequences.push_back(sequence);
                    name.clear();
                    content.clear();
                }
                name = line.substr(1);
            } else {
                content += line;
            }
        }
        
        if (!name.empty()) {
            Sequence sequence(name, content);
            sequences.push_back(sequence);
        }
        return sequences;
    }
}