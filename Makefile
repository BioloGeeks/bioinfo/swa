
all: build-debug gdb-debug

build-debug:
	cmake -S . -B build/ -D CMAKE_BUILD_TYPE=Debug
	cmake --build build

run:
	./build/swa ./data/sample.fa

gdb-debug:
	gdb --args ./build/swa ./data/sample.fa
