#!/usr/bin/env bash
set -euo pipefail
pushd ./build
cmake .. 
make
popd > /dev/null
./build/swa ./data/sample.fa